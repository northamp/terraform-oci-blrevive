# Blacklight: Revive ARM server Terraform module

This Terraform module's goal is to deploy a Blacklight: Revive server on an Oracle Cloud Infrastructure ARM machine **that fits in the free tier**.

It is mostly based on a fork of [anotherglitchinthematrix/oci-free-tier-terraform-module](https://github.com/anotherglitchinthematrix/oci-free-tier-terraform-module) (commit [79ecc28](https://github.com/anotherglitchinthematrix/oci-free-tier-terraform-module/commit/79ecc28eb7a8e3f42cfa10571eaf2f259cf79488)).

## Usage

**For a more seeamless experience, use the [`deploy` project](https://gitlab.com/northamp/oci-blrevive/oci-blrevive-deploy) instead.**

### Variables preparation

Copy `terraform.tfvars.example` to i.e. `blrevive.tfvars`.

#### OCI Authentication

Browse to the Oracle Cloud Infrastructure dashboard, head over to [your profile/API keys](https://cloud.oracle.com/identity/domains/my-profile/api-keys), and click `Add API key`.

Make sure to download both public and private key files as PEM files, store them in a path you'll remember on the machine that'll run your Terraform module, and click `Add`. You should get a confirmation looking like the following:

![OCI-keys](docs/_images/oci-key.png)

Note down each parameters, and amend `blrevive.tfvars`'s `tenancy_ocid`, `user_ocid`, `fingerprint`, `region` with the values provided in that window, while `private_key_path` should be the path to the private key's PEM file you downloaded.

#### Compartment

Head over to the [compartment screen](https://cloud.oracle.com/identity/compartments), create one if necessary, and copy the OCID of the target compartment in `compartment_ocid`.

#### Instance sizing

As quoted by the original Terraform module, limitations are the following:

> **Ampere A1 Compute instances (Arm processor)**: All tenancies get the first 3,000 OCPU hours and 18,000 GB hours per month for free for VM instances using the VM.Standard.A1.Flex shape, which has an Arm processor. For Always Free tenancies, this is equivalent to 4 OCPUs and 24 GB of memory.

Since BL:RE requires at least 2 CPU cores and around 3GB of RAM, it would theoritically be possible to boot up two instances and stay in the free tier. However, to err on the side of caution, I would recommend starting only one instance with as much resources as possible (which is the default configuration).

One thing that can safely be changed however is `instance_name`, **but keep it short** since there are name constraints (`blre` does just fine).

**NOTE**: If you run into the following error while running `terraform plan`:

> **Error**: no Availability Domain match for AD number: 3

It means your region doesn't support more than one availability domain; change `availability_domain` to `1`.

#### SSH Authentication

[Generate a SSH key](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) if you don't have one already and add its fingerprint to `ssh_public_keys`.

### Running the module

Use the following commands:

```bash
terraform init -upgrade
terraform plan -var-file="blrevive.tfvars" -out tfplan
# If it looks right
terraform apply "tfplan"
```

Once it has done running, you should be able to SSH into the VM with the private key you specified. The instance's public IP can be found with:

```bash
terraform output instance_public_ips
```
