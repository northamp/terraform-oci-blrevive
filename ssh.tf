resource "tls_private_key" "atlas_ssh_key" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

output "private_key" {
  value     = tls_private_key.atlas_ssh_key.private_key_pem
  sensitive = true
}