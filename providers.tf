provider "oci" {
  tenancy_ocid     = var.ocid_tenancy
  user_ocid        = var.ocid_user
  fingerprint      = var.ocid_fingerprint
  private_key_path = var.ocid_private_key
  region           = var.ocid_region
}