# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/null" {
  version = "3.2.2"
  hashes = [
    "h1:m467k2tZ9cdFFgHW7LPBK2GLPH43LC6wc3ppxr8yvoE=",
    "zh:3248aae6a2198f3ec8394218d05bd5e42be59f43a3a7c0b71c66ec0df08b69e7",
    "zh:32b1aaa1c3013d33c245493f4a65465eab9436b454d250102729321a44c8ab9a",
    "zh:38eff7e470acb48f66380a73a5c7cdd76cc9b9c9ba9a7249c7991488abe22fe3",
    "zh:4c2f1faee67af104f5f9e711c4574ff4d298afaa8a420680b0cb55d7bbc65606",
    "zh:544b33b757c0b954dbb87db83a5ad921edd61f02f1dc86c6186a5ea86465b546",
    "zh:696cf785090e1e8cf1587499516b0494f47413b43cb99877ad97f5d0de3dc539",
    "zh:6e301f34757b5d265ae44467d95306d61bef5e41930be1365f5a8dcf80f59452",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:913a929070c819e59e94bb37a2a253c228f83921136ff4a7aa1a178c7cce5422",
    "zh:aa9015926cd152425dbf86d1abdbc74bfe0e1ba3d26b3db35051d7b9ca9f72ae",
    "zh:bb04798b016e1e1d49bcc76d62c53b56c88c63d6f2dfe38821afef17c416a0e1",
    "zh:c23084e1b23577de22603cff752e59128d83cfecc2e6819edadd8cf7a10af11e",
  ]
}

provider "registry.terraform.io/oracle/oci" {
  version     = "5.25.0"
  constraints = ">= 5.22.0"
  hashes = [
    "h1:8mBjwUUWY2qRHTY3IYEXnTyJvPQPNAhvYrw0xkYOUZA=",
    "zh:11bfa7b4dabbd64aff8203995cdbdbb816c2c980b3cbe5cc9f55fabbc34ea70a",
    "zh:20f2124c52566f92324cfacfd96a14f3af0b3b0f8784a1930e6ddc3d24f697bb",
    "zh:383c4a18cfc6a66cf01bbed5295e3b7913b9551e732fb06faaa8ff5dcdf279ea",
    "zh:5d3689ffdb06984326d76acb69d509f51fc3b54ff4347a49a1f6f75a7404ec8b",
    "zh:68a28901e50c7be2ceddd3e4432b0ce78055077dd99ebbc39ea0139a06cea627",
    "zh:6e3093b2eec0fd495d0bc9c4bd172ffdbe864d257dea28f1eda2069dcfca1823",
    "zh:810229cb987c7752c3513b6d39b69f76fa120dde4759ac2ec5b4cd2266243588",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a44c6a771baae6c0aa0d476a2f1364b4cbeb824e72d6b717d62cf9ce01b33a8f",
    "zh:b557b268fb3f96a3da0a3d1b2ccea547a38034b546bba283a5f1bb8a2738e74c",
    "zh:dfaccdc3ea84725e6e98e11a48731fb535cfa5d03cbcedb2f478b8c2ed42fea3",
    "zh:e7a0c1ae0ae0fb8fbe78602a5e6e05ff0b25f179e2966b41801925f08b85856a",
    "zh:ef4b8c7cc88ff0be8c817e11c69fd01911d9e30ebdb6d75f2123e25b1547946d",
    "zh:f00515df4dad7c125d49dd3eb03701c21ded437774c5ce0339f5868277fb848d",
    "zh:fc1abb736fd67eb16003eae5341ac95746aa468a966d9990ed22bccd81944635",
  ]
}
