resource "oci_core_instance" "atlas_instance" {
  count               = var.num_instances
  availability_domain = data.oci_identity_availability_domain.ad.name
  compartment_id      = var.ocid_compartment
  display_name        = format("%s${count.index}", replace(title(var.instance_name), "/\\s/", ""))
  shape               = var.instance_shape

  shape_config {
    ocpus         = var.instance_ocpus
    memory_in_gbs = var.instance_shape_config_memory_in_gbs
  }

  create_vnic_details {
    subnet_id                 = oci_core_subnet.atlas_subnet.id
    display_name              = format("%sVNIC", replace(title(var.instance_name), "/\\s/", ""))
    assign_public_ip          = true
    assign_private_dns_record = true
    hostname_label            = format("%s${count.index}", lower(replace(var.instance_name, "/\\s/", "")))
  }

  source_details {
    source_type             = var.instance_source_type
    source_id               = var.instance_image_ocid[var.ocid_region]
    boot_volume_size_in_gbs = var.boot_volume_size_in_gbs
  }

  metadata = {
    ssh_authorized_keys = tls_private_key.atlas_ssh_key.public_key_openssh
  }

  timeouts {
    create = "60m"
  }
}

data "oci_core_instance_devices" "atlas_instance_devices" {
  count       = var.num_instances
  instance_id = oci_core_instance.atlas_instance[count.index].id
}

output "instance_private_ips" {
  value = [oci_core_instance.atlas_instance.*.private_ip]
}

output "instance_public_ips" {
  value = [oci_core_instance.atlas_instance.*.public_ip]
}

output "boot_volume_ids" {
  value = [oci_core_instance.atlas_instance.*.boot_volume_id]
}

output "instance_devices" {
  value = [data.oci_core_instance_devices.atlas_instance_devices.*.devices]
}
