resource "oci_core_vcn" "atlas_vcn" {
  cidr_block     = "10.1.0.0/16"
  compartment_id = var.ocid_compartment
  display_name   = format("%sVCN", replace(title(var.instance_name), "/\\s/", ""))
  dns_label      = format("%svcn", lower(replace(var.instance_name, "/\\s/", "")))
}

resource "oci_core_security_list" "atlas_security_list" {
  compartment_id = var.ocid_compartment
  vcn_id         = oci_core_vcn.atlas_vcn.id
  display_name   = format("%sSecurityList", replace(title(var.instance_name), "/\\s/", ""))

  # Allow outbound TCP port 53 (DNS)
  egress_security_rules {
    protocol    = 6
    destination = "0.0.0.0/0"
    stateless   = false
    tcp_options {
      min = 53
      max = 53
    }
  }

  # Allow outbound UDP port 53 (DNS)
  egress_security_rules {
    protocol    = 17
    destination = "0.0.0.0/0"
    stateless   = false
    udp_options {
      min = 53
      max = 53
    }
  }

  # Allow outbound UDP port 67 (DHCPDISCOVER)
  egress_security_rules {
    protocol    = 17
    destination = "0.0.0.0/0"
    stateless   = false
    udp_options {
      min = 67
      max = 67
    }
  }

  # Allow outbound TCP port 80 (HTTP)
  egress_security_rules {
    protocol    = 6
    destination = "0.0.0.0/0"
    stateless   = false
    tcp_options {
      min = 80
      max = 80
    }
  }

  # Allow outbound TCP port 443 (HTTPS)
  egress_security_rules {
    protocol    = 6
    destination = "0.0.0.0/0"
    stateless   = false
    tcp_options {
      min = 443
      max = 443
    }
  }

  # Allow outbound UDP port 123 (NTP)
  egress_security_rules {
    protocol    = 17
    destination = "0.0.0.0/0"
    stateless   = false
    udp_options {
      min = 123
      max = 123
    }
  }

  # Allow inbound TCP port 22 (SSH)
  ingress_security_rules {
    protocol  = 6
    source    = "0.0.0.0/0"
    stateless = false
    tcp_options {
      min = 22
      max = 22
    }
  }

  # Allow inbound TCP port 7777 (BLRevive)
  ingress_security_rules {
    protocol  = 6
    source    = "0.0.0.0/0"
    stateless = false
    tcp_options {
      min = 7777
      max = 7777
    }
  }

  # Allow inbound UDP port 7777 (BLRevive)
  ingress_security_rules {
    protocol  = 17
    source    = "0.0.0.0/0"
    stateless = false
    udp_options {
      min = 7777
      max = 7777
    }
  }

  # Allow inbound icmp traffic of a specific type
  ingress_security_rules {
    protocol  = 1
    source    = "0.0.0.0/0"
    stateless = false

    icmp_options {
      type = 3
      code = 4
    }
  }
}

resource "oci_core_internet_gateway" "atlas_internet_gateway" {
  compartment_id = var.ocid_compartment
  display_name   = format("%sIGW", replace(title(var.instance_name), "/\\s/", ""))
  vcn_id         = oci_core_vcn.atlas_vcn.id
}

resource "oci_core_default_route_table" "default_route_table" {
  manage_default_resource_id = oci_core_vcn.atlas_vcn.default_route_table_id
  display_name               = "DefaultRouteTable"

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.atlas_internet_gateway.id
  }
}

resource "oci_core_subnet" "atlas_subnet" {
  availability_domain = data.oci_identity_availability_domain.ad.name
  cidr_block          = "10.1.20.0/24"
  display_name        = format("%sSubnet", replace(title(var.instance_name), "/\\s/", ""))
  dns_label           = format("%ssubnet", lower(replace(var.instance_name, "/\\s/", "")))
  security_list_ids   = [oci_core_security_list.atlas_security_list.id]
  compartment_id      = var.ocid_compartment
  vcn_id              = oci_core_vcn.atlas_vcn.id
  route_table_id      = oci_core_vcn.atlas_vcn.default_route_table_id
  dhcp_options_id     = oci_core_vcn.atlas_vcn.default_dhcp_options_id
}

data "oci_identity_availability_domain" "ad" {
  compartment_id = var.ocid_tenancy
  ad_number      = var.availability_domain
}
